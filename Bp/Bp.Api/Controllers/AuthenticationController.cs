﻿using Bp.Api.Jwt;
using Bp.Model;
using Bp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Bp.Api.Controllers
{
    public class AuthenticationRequest
    {
        [JsonPropertyName("identification")]
        public string Identification { get; set; }
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }

    [ApiController]
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        private readonly ILogger<AuthenticationController> logger;
        private readonly ICostumerRepository repository;
        private IRepository<Costumer> repositoryCostumerGeneric;
        public AuthenticationController(ILogger<AuthenticationController> logger, ICostumerRepository repository, IRepository<Costumer> repositoryCostumerGeneric)
        {
            this.logger = logger;
            this.repository = repository;
            this.repositoryCostumerGeneric = repositoryCostumerGeneric;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] AuthenticationRequest data)
        {
            try
            {
                await repository.RegisterAsync(data.Identification, data.Password);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message.ToUpper());
            }
                       
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public IActionResult Login([FromBody] AuthenticationRequest data)
        {
            try
            {
                IActionResult response = Unauthorized();
                if (repository.Login(data.Identification, data.Password)) {
                    response = Ok(new
                    {
                        token = TokenGenerator.GenerateTokenJwt(data.Identification),
                        id = repositoryCostumerGeneric.GetByColumn("Identification", data.Identification).First().Id
                    });
                }
                return response;
            }
            catch (System.Exception ex)
            {
                return Problem(ex.Message.ToUpper());
            }
        }

        [HttpPost]
        public IActionResult getUserCanCreateCostumers()
        {
            ClaimsPrincipal currentUser = this.User;
            var id = currentUser.FindFirst(ClaimTypes.Name).Value;
            Costumer c = repositoryCostumerGeneric.GetByColumn("Identification", id).First();
            return Ok(c.Id);
        }

    }
}
