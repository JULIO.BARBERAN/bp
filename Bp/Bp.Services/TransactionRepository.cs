﻿using Bp.Data;
using Bp.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bp.Services
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        private readonly BpContext context;
        public TransactionRepository(BpContext context) : base(context)
        {
            this.context = context; 
        }

        public async Task<Transaction> CreateMovement(Transaction transaction)
        {
            try
            {
                Transaction t = this.context.Transaction.
                Where(x => x.AccountId == transaction.AccountId).
                OrderByDescending(u => u.Id).
                FirstOrDefault();
                if (t != null && transaction.Operation.Equals(Operation.CREDIT))
                {
                    transaction.Balance = t.Balance + transaction.Amount;
                }
                else if (t != null && transaction.Operation.Equals(Operation.DEBIT))
                {
                    transaction.Balance = t.Balance - transaction.Amount;
                }

                Validate(transaction);

                await Create(transaction);
                return transaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }
        public async Task<Transaction> UpdateMovement(Transaction transaction)
        {
            try
            {
                List<Transaction> transacciones = this.context.Transaction.
                Where(x => x.AccountId == transaction.AccountId && x.DateTransaction >= transaction.DateTransaction && x.Id != transaction.Id).
                OrderByDescending(u => u.Id).ToList();

                Transaction t = this.context.Transaction.
                Where(x => x.Id == transaction.Id).First();

                context.Entry(t).State = EntityState.Detached;

                if (transaction.Operation.Equals(Operation.CREDIT))
                {
                    transaction.Balance = (t.Balance - t.Amount) + transaction.Amount;
                }
                else if (transaction.Operation.Equals(Operation.DEBIT))
                {
                    transaction.Balance = (t.Balance - t.Amount) - transaction.Amount;
                }

                if (transaction.Balance < 0) {
                    throw new Exception("Operación no permitida");
                }

                await Update(transaction);
                
                foreach (var tp in transacciones)
                {
                    tp.Balance = transaction.Balance + tp.Amount;
                    await Update(tp);
                }
                
                return transaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<Transaction> DeleteMovement(int Id)
        {
            Transaction t = this.context.Transaction.
                Where(x => x.Id == Id).First();

            context.Entry(t).State = EntityState.Detached;

            Transaction lastTransaction = this.context.Transaction.
            Where(x => x.AccountId == t.AccountId).
            OrderByDescending(u => u.Id).First();

            if (lastTransaction.Id != t.Id)
            {
                throw new Exception("Operación no permitida, solo se puede borrar la ultima transacción");
            }

            await Delete(Id);
            return t;

        }
        public void Validate(Transaction transaction)
        {
            if (transaction.Balance < 0)
            {
                throw new Exception("Saldo no disponible");
            }

            Parameter p = this.context.Parameter.Where(x => x.LimitAmoumtByDay).
                FirstOrDefault();

            decimal c = this.context.Transaction.ToList().Where(x => x.DateTransaction >= StartOfDay(DateTime.Now)
            && x.DateTransaction <= EndOfDay(DateTime.Now) && x.AccountId == transaction.AccountId && x.Operation.Equals(Operation.DEBIT))
                .Sum(c => c.Amount);

            if (transaction.Operation.Equals(Operation.DEBIT) && p.Value < ((double)c + (double)transaction.Amount))
            {
                throw new Exception("Valor tope alcanzado");
            }
        }
        public DateTime EndOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
        }
        public DateTime StartOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
        }
        public List<ReportResponse> ReporteTransaccional(ReportRequest request) { 
            var cliente = context.Costumer.Where(x => x.Id == request.Id).First(); 
            List<Account> cuentas = context.Account.Where(x => x.Costumer.Id == request.Id).ToList();
            List<Transaction> movimientos = new List<Transaction>();
            List<ReportResponse> resultado = new List<ReportResponse>();
            foreach (var cuenta in cuentas)
            {
                movimientos.AddRange(this.context.Transaction.Where(x => x.DateTransaction >= request.FechaInicio
                    && x.DateTransaction <= request.FechaFin && x.AccountId == cuenta.Id).ToList());
            }
            foreach (var movimiento in movimientos)
            {
                ReportResponse r = new ReportResponse();
                r.Fecha = movimiento.DateTransaction;
                r.SaldoDisponible = movimiento.Balance;
                r.SaldoInicial = (movimiento.Operation.Equals(Operation.CREDIT)) ? movimiento.Balance - movimiento.Amount : movimiento.Balance + movimiento.Amount;
                r.Movimiento = movimiento.Amount;
                r.Tipo = Enum.GetName(typeof(TypeAccount),movimiento.Account.TypeAccount);
                r.Cuenta = movimiento.Account.Number;
                r.Cliente = cliente.Name;
                resultado.Add(r);
            }

            return resultado.OrderByDescending(x => x.Fecha).ToList();
        }

    }
}
