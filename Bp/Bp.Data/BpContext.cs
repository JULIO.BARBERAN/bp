﻿using Bp.Model;
using Microsoft.EntityFrameworkCore;
using System;

namespace Bp.Data
{
    public class BpContext : DbContext
    {
        public BpContext(DbContextOptions<BpContext> options)
            : base(options)
        {
        }

        public DbSet<Costumer> Costumer { get; set; }
        public DbSet<Account> Account { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<Parameter> Parameter { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Account>().
                HasOne(e => e.Costumer).
                WithMany(d => d.Accounts)
                .HasForeignKey(e => e.CostumerId);

            builder.Entity<Account>()
            .Property(p => p.OpeningBalance)
            .HasColumnType("decimal(18,4)");

            builder.Entity<Account>()
            .Property(p => p.RowVersion)
            .IsRowVersion();

            builder.Entity<Account>()
            .HasIndex(p => new { p.Number, p.TypeAccount })
            .IsUnique(true);

            builder.Entity<Transaction>().
                HasOne(e => e.Account).
                WithMany(d => d.Transactions)
                .HasForeignKey(e => e.AccountId);

            builder.Entity<Transaction>()
            .Property(p => p.Balance)
            .HasColumnType("decimal(18,4)");

            builder.Entity<Transaction>()
            .Property(p => p.Amount)
            .HasColumnType("decimal(18,4)");

            builder.Entity<Transaction>()
            .Property(p => p.RowVersion)
            .IsRowVersion();

            builder.Entity<Parameter>()
            .Property(p => p.Value)
            .HasColumnType("decimal(18,4)");

            base.OnModelCreating(builder);
            builder.Entity<Costumer>()
            .HasData(
            new Costumer
            {
                Id = -1,
                Name = "John Doe",
                gender = Gender.MALE,
                Age = 30,
                Identification = "1312738493",
                Address = "Westchester County, New York",
                Phone = "+1 (917) 500-8088",
                State =  State.INACTIVE,
                CanCreateCostumers = true,
            },
            new Costumer
            {
                Id = -2,
                Name = "Diana Lewis",
                gender = Gender.FEMALE,
                Age = 26,
                Identification = "9652738443",
                Address = "Long Island, New York",
                Phone = "+1 (997) 963-8087",
                State = State.INACTIVE,
                CanCreateCostumers = false,
            });

            builder.Entity<Parameter>()
            .HasData(
            new Parameter
            {
                Id = -1,
                LimitAmoumtByDay = true,
                Value = 1000,
            });
        }
    }
}
