﻿using AutoMapper;
using Bp.Model;
using Bp.Model.Dto;
using Bp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Bp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CostumerController : ControllerBase
    {
        private IRepository<Costumer> repository;
        public CostumerController(IRepository<Costumer> repository)
        { 
            this.repository = repository; 
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetAll() {
            return Ok(JsonSerializer.Serialize(repository.GetAll()));
        }

        [HttpGet("GetById")]
        public IActionResult GetById(int Id)
        {
            return Ok(JsonSerializer.Serialize(repository.GetByColumn("Id", Id.ToString()).First()));
        }

        [HttpGet]
        [Route("GetByColumn")]
        public IActionResult GetByColumn(string Column, string value)
        {
            return Ok(JsonSerializer.Serialize(repository.GetByColumn(Column, value)));
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CostumerDto model) {
            try
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<CostumerDto, Costumer>());
                var e = new Mapper(config).Map<Costumer>(model);

                IEnumerable<Costumer> l = repository.GetByColumn("Identification", model.Identification);
                if (l.ToList().Count > 0)
                {
                    return Problem("Identificación esta repetido");
                }    
                await repository.Create(e);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return Problem("Se presento un error en el servidor");
            }

        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] CostumerDto model) {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<CostumerDto, Costumer>());
            var e = new Mapper(config).Map<Costumer>(model);
            Costumer c = repository.GetById(model.Id).Result;
            e.Password = c.Password;
            await repository.Update(e);
            return Ok(true);

        }

        [HttpDelete("{Id}")]
        public async Task DeleteAsync(int Id)
        {
            try
            {
                await repository.Delete(Id);
            }
            catch (Exception ex)
            {
            }
        }

    }
}
