using Bp.Data;
using Bp.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Text.Json.Serialization;

namespace Bp.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            AppConfiguration.Instance.Name = configuration.GetValue<string>("JWT:Name");
            AppConfiguration.Instance.Jwt.SecretKey = configuration.GetValue<string>("JWT:SecretKey");
            AppConfiguration.Instance.Jwt.AudienceToken = configuration.GetValue<string>("JWT:AudienceToken");
            AppConfiguration.Instance.Jwt.ExpireTime = configuration.GetValue<Int32>("JWT:ExpireTime");
            AppConfiguration.Instance.Jwt.IssuerToken = configuration.GetValue<string>("JWT:IssuerToken");
            string corsOrigen = configuration.GetValue<string>("CorsOrigen");
            AppConfiguration.Instance.CorsOrigen = (!String.IsNullOrEmpty(corsOrigen)) ? corsOrigen.Split(',') : new[] { "http://localhost:4200" };
        }

        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "End point Bp";
            });
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("CORSPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {

                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {

                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration.GetValue<string>("Jwt:IssuerToken"),
                    ValidAudience = Configuration.GetValue<string>("Jwt:AudienceToken"),
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetValue<string>("Jwt:SecretKey"))),
                    ClockSkew = TimeSpan.Zero
                };
            });


            services.AddControllers().AddJsonOptions(x =>
    x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve);
            services.AddDbContext<BpContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddMvc();
            services.AddScoped<ICostumerRepository, CostumerRepository>();         
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            AddSwagger(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("CORSPolicy");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Foo API V1");
            });

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
