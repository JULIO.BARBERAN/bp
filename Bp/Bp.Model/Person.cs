﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bp.Model
{
    public enum Gender
    {
        FEMALE,
        MALE
    }
    public class Person
    {
        [Required]
        [MaxLength(12), MinLength(12)]
        public string Identification { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public Gender gender { get; set; }
        [Required]
        public int Age { get; set; }
        [Required]
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
