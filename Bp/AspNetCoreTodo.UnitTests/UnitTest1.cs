using Bp.Api.Controllers;
using Bp.Model;
using Bp.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace AspNetCoreTodo.UnitTests;

public class UnitTest1
{
    [Fact]
    public void TestCostumers()
    {
        var mock = new Mock<IRepository<Costumer>>();
        List<Costumer> l = new List<Costumer>();
        Costumer c = new Costumer
        {
            Id = -1,
            Name = "John Doe",
            gender = Gender.MALE,
            Age = 30,
            Identification = "1312738493",
            Address = "Westchester County, New York",
            Phone = "+1 (917) 500-8088",
            State = State.INACTIVE,
            CanCreateCostumers = true,
        };
        l.Add(c);
        mock.Setup(p => p.GetByColumn("Identification", "1312738493")).Returns(l);
        CostumerController home = new CostumerController(mock.Object);
        var result = home.GetByColumn("Identification", "1312738493");
        Assert.NotNull(result);
    }

    [Fact]
    public void TestCostumers2()
    {
        var mock = new Mock<IRepository<Costumer>>();
        List<Costumer> l = new List<Costumer>();
        Costumer c = new Costumer
        {
            Id = -1,
            Name = "John Doe",
            gender = Gender.MALE,
            Age = 30,
            Identification = "1312738493",
            Address = "Westchester County, New York",
            Phone = "+1 (917) 500-8088",
            State = State.INACTIVE,
            CanCreateCostumers = true,
        };
        l.Add(c);
        mock.Setup(p => p.GetByColumn("Identification", "1312738493")).Returns(l);
        CostumerController home = new CostumerController(mock.Object);
        var result = home.GetByColumn("Identification", "1312738493");
        var okResult = Assert.IsAssignableFrom<IActionResult>(result);
        Assert.NotNull(okResult);
    }
}