﻿using Bp.Model;
using Bp.Model.Dto;
using Bp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace Bp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TransactionController : ControllerBase
    {
        private readonly ILogger<TransactionController> logger;
        private readonly ITransactionRepository repository;
        private IRepository<Transaction> repositoryGeneric;
        private IRepository<Account> repositoryAccountGeneric;
        private IRepository<Costumer> repositoryCostumerGeneric;
        public TransactionController(ILogger<TransactionController> logger, ITransactionRepository repository, IRepository<Transaction> repositoryGeneric, IRepository<Account> repositoryAccountGeneric, IRepository<Costumer> repositoryCostumerGeneric)
        {
            this.logger = logger;
            this.repository = repository;
            this.repositoryGeneric = repositoryGeneric; 
            this.repositoryAccountGeneric = repositoryAccountGeneric;
            this.repositoryCostumerGeneric = repositoryCostumerGeneric;
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetAll() {
            ClaimsPrincipal currentUser = this.User;
            var id = currentUser.FindFirst(ClaimTypes.Name).Value;
            Costumer cliente = repositoryCostumerGeneric.GetByColumn("Identification", id).First();
            List<Account> cuentas = repositoryAccountGeneric.GetByColumn("CostumerId", cliente.Id.ToString()).ToList();
            List<Transaction> movimientos = new List<Transaction>();
            foreach (var cuenta in cuentas)
            {
                movimientos.AddRange(repositoryGeneric.GetByColumn("AccountId", cuenta.Id.ToString()).ToList());
            }
            return Ok(JsonSerializer.Serialize(movimientos));
        } 

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] TransactionDto dto) {
            try
            {
                Transaction t = new Transaction();
                t.DateTransaction = DateTime.Now;
                t.AccountId = repositoryAccountGeneric.GetById(dto.AccountId).Result.Id;
                t.Amount = dto.Amount;
                t.Operation = dto.Operation;
                await this.repository.CreateMovement(t);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }

        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] TransactionDto dto)
        {
            try
            {
                Transaction t = new Transaction();
                t.DateTransaction = dto.DateTransaction;
                t.AccountId = repositoryAccountGeneric.GetById(dto.AccountId).Result.Id;
                t.Amount = dto.Amount;
                t.Operation = dto.Operation;
                t.Id = dto.Id;
                t.Balance = dto.Balance;
                t.RowVersion = dto.RowVersion;
                await this.repository.UpdateMovement(t);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAsync(int Id)
        {
            try
            {
                await repository.DeleteMovement(Id);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost]
        [Route("reporte")]
        public IActionResult Reporte([FromBody] ReportRequest request)
        {
            return Ok(JsonSerializer.Serialize(repository.ReporteTransaccional(request)));
        }

    }
}
