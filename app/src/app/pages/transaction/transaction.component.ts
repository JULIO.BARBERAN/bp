import { Component, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import { BaseService, Operation } from 'src/app/shared/services/base.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent {
  @ViewChild('gridContainer', { static: false }) dataGrid: DxDataGridComponent | undefined;
  dataSource: CustomStore;
  costumerStore: CustomStore;
  operations: Operation[];
  constructor(private serviceAplication: BaseService) {
    this.operations = serviceAplication.getOperations();
    this.dataSource = new CustomStore({
      key: 'Id',
      load: () => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Transaction`),
      insert: (values) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Transaction`, 'POST', {
        values: JSON.stringify(values),
      }),
      update: (key, values) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Transaction`, 'PUT', {
        values: JSON.stringify(values),
      }),
      remove: (key) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Transaction/${key}`, 'DELETE', {
        key,
      }),
    });

    this.costumerStore = new CustomStore({
      key: 'Id',
      byKey: (key) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Account/GetByColumn?Column=CostumerId&&value=` +localStorage.getItem("id")),
      load: () => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Account/GetByColumn?Column=CostumerId&&value=`+localStorage.getItem("id")),
    });
  }

  onClickRefresh(e: any) {
    this.dataGrid?.instance.refresh();
  }

  onRowUpdating = (e: any) => {
    e.newData = Object.assign({}, e.oldData, e.newData);
  }

  onToolbarPreparing(e: any) {
    e.toolbarOptions.items.unshift( {
      location: 'after',
      widget: 'dxButton',
      options: {
        icon: 'refresh',
        onClick: this.onClickRefresh.bind(this)
      }
    });
  }

}
