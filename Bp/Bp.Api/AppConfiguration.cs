﻿using Bp.Api.Jwt;
using System;

namespace Bp.Api
{
    public class AppConfiguration : IAppConfiguration    
    {
        private static readonly Lazy<AppConfiguration>
            lazy = new Lazy<AppConfiguration>(() => new AppConfiguration());

        public static AppConfiguration Instance { get { return lazy.Value; } }

        private AppConfiguration()
        {
            Jwt = new JwtOption();
        }
        public string Name { get; set; }
        public JwtOption Jwt { get; set; }
        public string[] CorsOrigen { get; set; }
        public string KeyLimitBalance { get; set; }
    }
}
