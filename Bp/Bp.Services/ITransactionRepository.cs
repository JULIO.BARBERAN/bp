﻿using Bp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bp.Services
{
    public interface ITransactionRepository
    {
        void Validate(Transaction transactionn);
        Task<Transaction> CreateMovement(Transaction transaction);
        Task<Transaction> UpdateMovement(Transaction transaction);
        List<ReportResponse> ReporteTransaccional(ReportRequest request);
        Task<Transaction> DeleteMovement(int Id);

    }
}
