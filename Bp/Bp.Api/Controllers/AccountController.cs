﻿using AutoMapper;
using Bp.Model;
using Bp.Model.Dto;
using Bp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace Bp.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AccountController : ControllerBase
    {
        private IRepository<Account> repository;
        private IRepository<Transaction> repositoryTransaction;
        private readonly ILogger<AccountController> logger;
        private IRepository<Costumer> repositoryCostumerGeneric;
        public AccountController(IRepository<Account> repository, IRepository<Transaction> repositoryTransaction, ILogger<AccountController> logger, IRepository<Costumer> repositoryCostumerGeneric)
        { 
            this.repository = repository;
            this.repositoryTransaction = repositoryTransaction;
            this.repositoryCostumerGeneric = repositoryCostumerGeneric;
            this.logger = logger;
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetAll() => Ok(JsonSerializer.Serialize(repository.GetAll()));

        [HttpGet]
        [Route("GetByColumn")]
        public IActionResult GetByColumn(string Column, string value) {
            return Ok(JsonSerializer.Serialize(repository.GetByColumn(Column, value)));
        } 

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody]AccountDto model) {
            try
            {
                var config = new MapperConfiguration(cfg =>
                    cfg.CreateMap<AccountDto, Account>()
                );
                
                IEnumerable<Account> l = repository.GetByColumn("Number", model.Number);
                if (l.Where(x => x.TypeAccount == model.TypeAccount).ToList().Count > 0) {
                    throw new Exception("Numero esta repetido");
                }

                var e = new Mapper(config).Map<Account>(model);
                await repository.Create(e);
                Transaction t = new Transaction();
                t.Account = e;
                t.Amount = t.Balance = e.OpeningBalance;
                t.Operation = Operation.CREDIT;
                t.DateTransaction = DateTime.Now;
                await repositoryTransaction.Create(t);  
                return Ok(true);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
            
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] AccountDto model)
        {
            try
            {
                var config = new MapperConfiguration(cfg =>
                    cfg.CreateMap<AccountDto, Account>()
                );

                var l = repository.GetByColumn("Id", model.Id.ToString()).First();
                if (l.OpeningBalance != model.OpeningBalance) {
                    return Problem("Deposito inicial no puede modificarse");
                }
                var e = new Mapper(config).Map<Account>(model);
                await repository.Update(e);
                return Ok(e);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
            
        }

        [HttpDelete("{Id}")]
        public async Task DeleteAsync(int Id)
        {
            try
            {
                await repository.Delete(Id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }
        }
    }
}
