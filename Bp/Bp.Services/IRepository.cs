﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bp.Services
{
    public interface IRepository<TEntity>
        where TEntity : class
    {
        List<TEntity> GetAll();

        Task<TEntity> GetById(int id);

        Task Create(TEntity entity);

        Task Update(TEntity entity);

        Task Delete(int id);
        IEnumerable<TEntity> GetByColumn(string property, string value);
    }
}
