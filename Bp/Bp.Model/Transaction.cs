﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Bp.Model
{
    public enum Operation
    {
        DEBIT,
        CREDIT
    }
    public class Transaction : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public Operation Operation { get; set; }
        [Required]
        [Range(0, Double.MaxValue)]
        public decimal Balance { get; set; }
        [Required]
        [Range(0, Double.MaxValue)]
        public decimal Amount { get; set; }
        [Required]
        public DateTime DateTransaction { get; set; }
        [Required]
        [ForeignKey("Account")]
        public int AccountId { get; set; }
        public Account Account { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
