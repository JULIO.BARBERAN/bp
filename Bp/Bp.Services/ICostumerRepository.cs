﻿using Bp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bp.Services
{
    public interface ICostumerRepository
    {
        Boolean Login(string Identification, string Password);
        Task<bool> RegisterAsync(string Identification, string Password);
    }
}
