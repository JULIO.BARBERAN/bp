import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DxDataGridComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { AuthService } from 'src/app/shared/services';
import { BaseService, Gender, StateCostumer } from 'src/app/shared/services/base.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-costumer',
  templateUrl: './costumer.component.html',
  styleUrls: ['./costumer.component.scss']
})
export class CostumerComponent {
  @ViewChild('gridContainer', { static: false }) dataGrid: DxDataGridComponent | undefined;
  dataSource: CustomStore;
  stateCostumer: StateCostumer[];
  genders: Gender[];
  constructor(private serviceAplication: BaseService, private _auth: AuthService, private router: Router) { 
    this.stateCostumer = serviceAplication.getStateCostumer();
    this.genders = serviceAplication.getGender();
    this.dataSource = new CustomStore({
      key: 'Id',
      load: () => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Costumer`),
      insert: (values) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Costumer`, 'POST', {
        values: JSON.stringify(values),
      }),
      update: (key, values) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Costumer`, 'PUT', {
        values: JSON.stringify(values),
      }),
      remove: (key) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Costumer/${key}`, 'DELETE', {
        key,
      }),
    });
  }
  
  onClickRefresh(e: any) {
    this.dataGrid?.instance.refresh();
  }

  onRowUpdating = (e: any) => {
    e.newData = Object.assign({}, e.oldData, e.newData);
  }

  onToolbarPreparing(e: any) {
    e.toolbarOptions.items.unshift( {
      location: 'after',
      widget: 'dxButton',
      options: {
        icon: 'refresh',
        onClick: this.onClickRefresh.bind(this)
      }
    });
  }

}
