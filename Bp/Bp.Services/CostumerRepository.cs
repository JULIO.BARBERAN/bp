﻿using Bp.Data;
using Bp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Bp.Services
{
    public class CostumerRepository : Repository<Costumer>, ICostumerRepository
    {
        private readonly BpContext context;
        public CostumerRepository(BpContext context) : base(context)
        {
            this.context = context;
        }
        public static string ComputeSha256Hash(string rawData)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
        public bool Login(string Identification, string Password)
        {
            var l = this.context.Costumer.
                Where(x => x.Identification == Identification
                && x.Password == ComputeSha256Hash(Password)).Count();
            return l > 0;
        }

        public async Task<bool> RegisterAsync(string identification, string password)
        {
            try
            {
                Costumer c = this.context.Costumer.Where(x => x.Identification.Equals(identification)).FirstOrDefault();
                if (c == null)
                    throw new Exception("Cliente no encontrado");
                if (c.Identification == identification && c.Password != null)
                    throw new Exception("Cliente ya registrado");
                
                c.Password = ComputeSha256Hash(password);
                await Update(c);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return true;
        }
    }
}
