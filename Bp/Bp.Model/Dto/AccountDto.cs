﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bp.Model.Dto
{
    public class AccountDto
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public TypeAccount TypeAccount { get; set; }
        public decimal OpeningBalance { get; set; }
        public StateAccount StateAccount { get; set; }
        public int CostumerId { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
