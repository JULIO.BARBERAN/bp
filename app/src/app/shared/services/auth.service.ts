import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface IUser {
  identification: string;
  password?: string
}

const defaultPath = '/';


@Injectable()
export class AuthService {
  private _user: IUser | undefined;
  get loggedIn(): boolean {
    return localStorage.getItem('JWT') !== null;
  }

  private _lastAuthenticatedPath: string = defaultPath;
  set lastAuthenticatedPath(value: string) {
    this._lastAuthenticatedPath = value;
  }

  constructor(private router: Router, private http: HttpClient) { }

  async logIn(identification: string, password: string) {
    let contentHeader: HttpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
    let data = {identification, password};
    return this.http.post(`${environment.apiUrl}/api/Authentication/login`, data, { headers: contentHeader, observe: 'response' });
  }

  get Token() {
    return localStorage.getItem('JWT');
  }

  async createAccount(identification: string, password: string) {
    let contentHeader: HttpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
    let data = {identification, password};
    return this.http.post(`${environment.apiUrl}/api/Authentication/register`, data, { headers: contentHeader, observe: 'response' });
  }

  async getUser(){
    let contentHeader = new HttpHeaders(
      {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.Token
      });
    return this.http.post(`${environment.apiUrl}/api/Authentication`, { headers: contentHeader, observe: 'response' });
  }

  async logOut() {
    localStorage.removeItem('JWT');
    localStorage.clear();
    this.router.navigate(['/login-form']);
  }
}

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const isLoggedIn = this.authService.loggedIn;
    const isAuthForm = [
      'login-form',
      'reset-password',
      'create-account',
      'change-password/:recoveryCode'
    ].includes(route.routeConfig?.path || defaultPath);

    if (isLoggedIn && isAuthForm) {
      this.authService.lastAuthenticatedPath = defaultPath;
      this.router.navigate([defaultPath]);
      return false;
    }

    if (!isLoggedIn && !isAuthForm) {
      this.router.navigate(['/login-form']);
    }

    if (isLoggedIn) {
      this.authService.lastAuthenticatedPath = route.routeConfig?.path || defaultPath;
    }

    return isLoggedIn || isAuthForm;
  }
}
