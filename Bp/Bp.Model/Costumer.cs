﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bp.Model
{
    public enum State
    {
        ACTIVE,
        INACTIVE
    }
    public class Costumer : Person, IEntity
    {
        [Key]
        public int Id { get; set; }
        public string Password { get; set; }
        [Required]
        public State State { get; set; }
        [Required]
        public bool CanCreateCostumers { get; set; }
        public ICollection<Account> Accounts { get; set; }
    }
}
