﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bp.Model.Dto
{
    public class TransactionDto
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public DateTime DateTransaction { get; set; }
        public int AccountId { get; set; }
        public Operation Operation { get; set; }
        public byte[] RowVersion { get; set; }

    }
}
