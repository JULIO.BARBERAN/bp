import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import 'devextreme/data/odata/store';
import { AuthService } from 'src/app/shared/services';
import { BaseService, StateAccount, TypeAccount } from 'src/app/shared/services/base.service';
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: 'account.component.html'
})

export class AccountComponent {
  @ViewChild('gridContainer', { static: false }) dataGrid: DxDataGridComponent | undefined;
  dataSource: CustomStore;
  costumerStore: CustomStore;
  typeAccounts: TypeAccount[];
  stateAccounts: StateAccount[];

  constructor(private serviceAplication: BaseService, private _auth: AuthService) {
    this.typeAccounts = serviceAplication.getTypeAccounts();
    this.stateAccounts = serviceAplication.getTStateAccounts();
    this.dataSource = new CustomStore({
      key: 'Id',
      load: () => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Account/GetByColumn?Column=CostumerId&&value=` +localStorage.getItem("id")),
      insert: (values) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Account`, 'POST', {
        values: JSON.stringify(values),
      }),
      update: (key, values) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Account`, 'PUT', {
        values: JSON.stringify(values),
      }),
      remove: (key) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Account/${key}`, 'DELETE', {
        key,
      }),
    });
    
    this.costumerStore = new CustomStore({
      key: 'Id',
      byKey: (key) => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Costumer/GetById?Id=` + key),
      load: () => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Costumer/GetById?Id=`+localStorage.getItem("id")),
    });
  }

  onClickRefresh(e: any) {
    this.dataGrid?.instance.refresh();
  }

  onRowUpdating = (e: any) => {
    e.newData = Object.assign({}, e.oldData, e.newData);
  }

  onToolbarPreparing(e: any) {
    e.toolbarOptions.items.unshift( {
      location: 'after',
      widget: 'dxButton',
      options: {
        icon: 'refresh',
        onClick: this.onClickRefresh.bind(this)
      }
    });
  }
}
