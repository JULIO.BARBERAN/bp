﻿namespace Bp.Api.Jwt
{
    public class JwtOption
    {
        public string SecretKey { get; set; }
        public string AudienceToken { get; set; }
        public string IssuerToken { get; set; }
        public int ExpireTime { get; set; }
    }
}
