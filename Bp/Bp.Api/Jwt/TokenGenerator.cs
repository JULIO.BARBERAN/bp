﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Bp.Api.Jwt
{
    internal static class TokenGenerator
    {
        public static string GenerateTokenJwt(string ID)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppConfiguration.Instance.Jwt.SecretKey));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, ID),
            };

            var token = new JwtSecurityToken(issuer: AppConfiguration.Instance.Jwt.IssuerToken,
                audience: AppConfiguration.Instance.Jwt.AudienceToken,
                claims: claims, expires: DateTime.Now.AddMinutes(AppConfiguration.Instance.Jwt.ExpireTime),
                signingCredentials: signingCredentials);

            return new JwtSecurityTokenHandler().WriteToken(token);


        }

    }
}
