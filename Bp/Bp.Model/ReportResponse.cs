﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bp.Model
{
    public class ReportResponse
    {
        public DateTime Fecha { get; set; }
        public String Cliente { get; set; }
        public String Cuenta { get; set; }
        public String Tipo { get; set; }
        public decimal SaldoInicial { get; set; }
        //public string Estado { get; set; }
        public decimal Movimiento { get; set; }
        public decimal SaldoDisponible { get; set; }
    }
}
