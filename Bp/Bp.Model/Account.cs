﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Bp.Model
{
    public enum TypeAccount
    {
        SAVINGS,
        CURRENT
    }

    public enum StateAccount
    {
        BLOCK,
        ACTIVE
    }
    public class Account : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }
        [Required]
        public string Number { get; set; }
        [Required]
        public TypeAccount TypeAccount { get; set; }
        [Required]
        [Range(0, Double.MaxValue)]
        public decimal OpeningBalance { get; set; }
        public StateAccount StateAccount { get; set; }
        [Required]
        [ForeignKey("Costumer")]
        public int CostumerId { get; set; }
        public Costumer Costumer { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        [JsonIgnore]
        public ICollection<Transaction> Transactions { get; set; }

    }
}
