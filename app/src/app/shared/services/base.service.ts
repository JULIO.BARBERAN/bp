import { formatDate } from "@angular/common";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";

export class Gender {
  id?: number;
  name?: string;
}

const gender: Gender[] = [{
  id: 0,
  name: 'MUJER',
  },
  {
    id: 1,
    name: 'HOMBRE',
  }
  ];

export class StateCostumer {
  id?: number;
  name?: string;
}

const stateCostumer: StateCostumer[] = [{
  id: 0,
  name: 'ACTIVO',
},
{
  id: 1,
  name: 'INACTIVO',
}
];

export class Operation {
  id?: number;
  name?: string;
}

const operations: Operation[] = [{
  id: 0,
  name: 'DEBITO',
},
{
  id: 1,
  name: 'CREDITO',
}
];

export class TypeAccount {
    id?: number;
    name?: string;
}
  
const typeAccounts: TypeAccount[] = [{
    id: 0,
    name: 'AHORROS',
  },
  {
    id: 1,
    name: 'CORRIENTE',
  }
];

export class StateAccount {
    id?: number;
    name?: string;
}
  
const stateAccounts: StateAccount[] = [{
    id: 0,
    name: 'BLOQUEADA',
  },
  {
    id: 1,
    name: 'ACTIVO',
  }
];

@Injectable({
    providedIn: 'root'
  })
  export class BaseService {
    requests: string[] = [];
    
    constructor(private http: HttpClient, private _auth: AuthService,) {

    }
    getTypeAccounts() {
        return typeAccounts;
    }

    getTStateAccounts() {
        return stateAccounts;
    }

    getOperations() {
      return operations;
    }

    getStateCostumer() {
      return stateCostumer;
    }

    getGender() {
      return gender;
    }

    sendRequest(url: string, method = 'GET', data: any = {}): any {
        let contentHeader = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._auth.Token
            });

        let result;
    
        switch (method) {
          case 'GET':
            result = this.http.get(url, { headers: contentHeader, observe: 'response' });
            break;
          case 'PUT':
            result = this.http.put(url, data.values, { headers: contentHeader, observe: 'response' });
            break;
          case 'POST':
            result = this.http.post(url, data.values, { headers: contentHeader, observe: 'response' });
            break;
          case 'DELETE':
            result = this.http.delete(url, { headers: contentHeader, observe: 'response' });
            break;
        }
        
        return result
          ?.toPromise()
          .then((x: any) => {
              return x.body;
          })
          .catch((e) => {
            throw new Error(e.error.detail);
          });
      }
    
      clearRequests() {
        this.requests = [];
      }
  }