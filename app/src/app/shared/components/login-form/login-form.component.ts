import { CommonModule } from '@angular/common';
import { Component, NgModule } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxLoadIndicatorModule } from 'devextreme-angular/ui/load-indicator';
import notify from 'devextreme/ui/notify';
import { AuthService } from '../../services';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
  loading = false;
  formData: any = {};

  constructor(private authService: AuthService, private router: Router) { }

  async onSubmit(e: Event) {
    e.preventDefault();
    const { identification, password } = this.formData;
    this.loading = true;
    let result = await this.authService.logIn(identification, password);
    result.toPromise().then(async (data: any) => {
        if(data.body) {
          localStorage.clear();
          localStorage.setItem('JWT', data.body.token);
          localStorage.setItem('id', data.body.id);
          this.router.navigate(['/']);
        }
    }).catch(e => {
      if(e.status == 401) {
        notify('Acceso no autorizado', 'warning', 10000);
      }
    });
    this.loading = false;
  }

  onCreateAccountClick = () => {
    this.router.navigate(['/create-account']);
  }
}
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DxFormModule,
    DxLoadIndicatorModule
  ],
  declarations: [ LoginFormComponent ],
  exports: [ LoginFormComponent ]
})
export class LoginFormModule { }
