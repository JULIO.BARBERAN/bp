﻿using Bp.Api.Jwt;

namespace Bp.Api
{
    public class IAppConfiguration 
    {
        string Name { get; }
        JwtOption Jwt { get; set; }
    }
}
