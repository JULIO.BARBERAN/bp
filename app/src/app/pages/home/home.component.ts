import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { AuthService } from 'src/app/shared/services';
import { BaseService } from 'src/app/shared/services/base.service';
import { environment } from 'src/environments/environment';

export class Request {
  Id?: Number;
  FechaInicio?: Date;
  FechaFin?: Date;
}

@Component({
  templateUrl: 'home.component.html',
  styleUrls: [ './home.component.scss' ]
})

export class HomeComponent {
  @ViewChild(DxFormComponent, { static: false }) form:DxFormComponent | undefined;
  request: Request | undefined;
  costumerStore: CustomStore;
  dataSource: any;
  constructor(private serviceAplication: BaseService, private http: HttpClient, private _auth: AuthService) {
    this.costumerStore = new CustomStore({
      key: 'Id',
      load: () => this.serviceAplication.sendRequest(`${environment.apiUrl}/api/Costumer`),
    });
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  buttonOptions: any = {
    text: 'Buscar',
    type: 'success',
    useSubmitBehavior: true,
  };

  async onFormSubmit(e:any) {
    e.preventDefault();
    this.request = this.form?.instance.option('formData');
    let result = await this.buscar(this.request);
    result.subscribe(x => {
      this.dataSource = x.body;
    });
  };

  async buscar(e:any) {
    let contentHeader = new HttpHeaders(
      {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._auth.Token
      });
    return this.http.post(`${environment.apiUrl}/api/Transaction/reporte`,  e, { headers: contentHeader, observe: 'response' });
  }
}
