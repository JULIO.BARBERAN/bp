export const navigation = [
  {
    text: 'Reporte',
    path: '/home',
    icon: 'home'
  },
  {
    text: 'Opciones',
    icon: 'folder',
    items: [
      {
        text: 'Clientes',
        path: '/costumer'
      },
      {
        text: 'Cuentas',
        path: '/account'
      }
      ,
      {
        text: 'Transacciones',
        path: '/transaction'
      }
    ]
  }
];
