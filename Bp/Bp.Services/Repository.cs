﻿using Bp.Data;
using Bp.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Bp.Services
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    { 
        private readonly BpContext _dbContext;

        public Repository(BpContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Create(TEntity entity)
        {
            await _dbContext.Set<TEntity>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var entity = await _dbContext.Set<TEntity>().FindAsync(id);
            _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public List<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().AsNoTracking().ToList();
        }

        public async Task<TEntity> GetById(int id)
        {
            try
            {
                return await _dbContext.Set<TEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
            }
            catch (Exception)
            {
                throw new Exception("Concurrent"); ;
            }
        }

        public async Task Update(TEntity entity)
        {
            try
            {
                _dbContext.Entry(entity).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new Exception("Concurrent");
            }
        }

        public IEnumerable<TEntity> GetByColumn(string property, string value)
        {
            PropertyInfo getter = typeof(TEntity).GetProperty(property);
            return GetAll().Where(x => getter.GetValue(x, null).ToString() == value);
        }
    }

}
